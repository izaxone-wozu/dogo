package com.izaxone.dogo.chat;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
// Any path within the MessageController will need the prefix /api to be accessed.
@RequestMapping("/api")


public class MessageController {

    @Autowired
    // The dao instance of the MessageRepository interface provides CRUD operations, implemented from the JpaRepository
    MessageRepository dao;

    // Create a Get method that maps to the route api/chat and returns JSON data.
    @GetMapping("/chat")
    public List<Message> getMessages() {
        List<Message> foundMessages = dao.findAll();
        return foundMessages;
    }

    // Get the specified message with an ID number
    @GetMapping("/chat/{id}")
    // PathVariable is a variable path
    public ResponseEntity<Message> getMessage(@PathVariable(value="id") Integer id) {
        // Find the message by ID (or return null), and set it to an instance of the Message model called 'foundMessage'
        Message foundMessage = dao.findById(id).orElse(null);

        // If no message found, return a placeholder message
        if (foundMessage == null) {
            return ResponseEntity.notFound().header("Message","Nothing found with that ID").build();

        }
        // Return the found message using ResponseEntity
        return ResponseEntity.ok(foundMessage);

    }

    @PostMapping("/chat")
    public ResponseEntity<Message> postMessage(@RequestBody Message message) {

        // Save to the DB using instance of repo interface
        Message createdMessage = dao.save(message);

        // RaspEntity crafts response to include correct status codes.
        return ResponseEntity.ok(createdMessage);

    }

    // Create a route that will allow an entry to be updated.
    // What mapping annotation should be used?
    @PutMapping("/chat/{id}")
    public ResponseEntity<Message> updateMessage(@PathVariable(value="id") Integer id, @RequestBody Message message) {

        // We're going to find that message by its ID, or return null
        System.out.println("Finding the message");
        Message foundMessage = dao.findById(id).orElse(null);

        System.out.println("PUT was called");

        // How to check if the entity already exists?
        if (foundMessage == null) {
            System.out.println("No message found. ");
            return ResponseEntity.notFound().header("Message", "Nothing found with that ID").build();

        } else {
            if (message.getName() != null) {
                // update the name
                foundMessage.setName(message.getName());
            }
            if (message.getContent() != null) {
                // update the content
                foundMessage.setContent(message.getContent());
            }
            dao.save(foundMessage);

        }

        // What should be returned from the method?
        return ResponseEntity.ok(foundMessage);
    }

    @DeleteMapping("/chat/{id}")
    public ResponseEntity<Message> deleteMessage(@PathVariable(value="id") Integer id) {
        Message foundMessage = dao.findById(id).orElse(null);

        System.out.println("DELETE was called");

        if (foundMessage == null) {
            System.out.println("No message found. ");
            return ResponseEntity.notFound().header("Message", "Nothing found with that ID").build();

        } else {
            dao.delete(foundMessage);
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("math/add/{x}/{y}")
    public int getMath(@PathVariable("x") int x,@PathVariable("y") int y ) {
        return x + y;
    }



}
