package com.izaxone.dogo.chat;

import org.springframework.data.jpa.repository.JpaRepository;

// Data Access Object: Mechanism which allows for the retrieval of these messages from the database


public interface MessageRepository extends JpaRepository<Message, Integer> {
}
