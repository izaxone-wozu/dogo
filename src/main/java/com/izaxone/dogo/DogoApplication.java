package com.izaxone.dogo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DogoApplication {

	public static void main(String[] args) {
		System.out.println("Starting up! ");
		SpringApplication.run(DogoApplication.class, args);
	}

}
