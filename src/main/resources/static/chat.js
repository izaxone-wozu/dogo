$(document).ready(function () {
    // when the button is clicked the values entered are gathered and inserted
    // into the string that will be the url route.
    $('#get').ready(function () {
        var url = 'api/chat';

        $.get(url, function (data) {
            var result = '';
            if (data.length < 1) {
                result = 'There is no data to display at this time.'
            }
            else {
                for (var i = 0; i < data.length; i++) {
                    result += '<strong>ID: </strong>' + data[i].id + '<br>';
                    result += '<strong>Name: </strong>' + data[i].name + '<br>';
                    result += '<strong>Content: </strong>' + data[i].content + '<br>';
                    result += '<br>'
                }
            }

            $('#result').html(result);
        })

    });

});